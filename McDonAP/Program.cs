﻿using System;

namespace McDonAP {
  enum Frisdranken {
    Water,
    Fanta,
    Cola,
    Geen
  }

  class Program {
    static double[] afrekeningen = new double[100];
    static void Main(string[] args) {
      while (true) {
        BestelHamburger();
      }
    }

    private static void BestelHamburger() {
      Console.WriteLine(@"*****************************************************
MENU
- Gewone hamburger: €5
- Fishburger: €6
- Veggieburger: €3
- Water €2
- Fanta €3
- Cola €3
- Frietjes: €2 per frietje");
      Console.WriteLine("BESTEL:");
      //burger
      string burger = "gewoon";
      bool shouldLoop = true;
      int counter = 0;
      while (shouldLoop) {
        Console.Write("Hamburger? (gewoon/fish/veggie): ");
        burger = Console.ReadLine();
        if (burger.Equals("gewoon") || burger.Equals("fish") ||
            burger.Equals("veggie")) {
          shouldLoop = false;
        }
      }
      int numFrietjes = BestelFrietjes();
      Frisdranken frisdrank = BestelFrisdrank();

      double prijs = Berekentotaal(burger: burger, numFrietjes: numFrietjes,
        frisdrank: frisdrank);

      Console.WriteLine("Totaal prijs = {0:F2} euro", prijs);
      Console.WriteLine("When paid, Press ENTER to continue...");
      Console.ReadLine();
      afrekeningen[counter] = prijs;
    }

    private static double Berekentotaal(string burger, int numFrietjes = 1,
      Frisdranken frisdrank = Frisdranken.Geen) {
      double totPrijs = 0.0;
      //burger
      switch (burger) {
        case "veggie":
          totPrijs += 3.0;
          break;
        case "fish":
          totPrijs += 6.0;
          break;
        default:
          //gewone burger
          totPrijs += 5.0;
          break;
      }
      //frietjes
      totPrijs += numFrietjes * 2.0;
      //drank
      if (frisdrank != Frisdranken.Geen) {
        if (frisdrank == Frisdranken.Water) {
          totPrijs += 2.0;
        } else {
          totPrijs += 3.0;
        }
      }
      //visualizeer bestelling
      Console.Write("VISUAL: ");
      switch (burger) {
        case "veggie":
          Console.BackgroundColor = ConsoleColor.DarkRed;
          break;
        case "fish":
          Console.BackgroundColor = ConsoleColor.DarkBlue;
          break;
        case "gewoon":
          Console.BackgroundColor = ConsoleColor.DarkGreen;
          break;
      }
      Console.Write("H");
      Console.ResetColor();
      for (int i = 0; i < numFrietjes; i++) {
        Console.Write("I");
      }
      switch (frisdrank) {
        case Frisdranken.Cola:
          Console.Write("C");
          break;
        case Frisdranken.Fanta:
          Console.Write("F");
          break;
        case Frisdranken.Water:
          Console.Write("W");
          break;
      }
      Console.WriteLine();

      
      //korting
      int korting = 0;
      string promoMsg = "";
      if (frisdrank != Frisdranken.Geen && numFrietjes > 0) {
        promoMsg = "Box (5 euro)";
        korting += 5;        
      } else if (frisdrank == Frisdranken.Water && burger.Equals("veggie") && numFrietjes == 1) {
        promoMsg = "Hipster (3 euro)";
        korting += 3;
      }
      if (promoMsg != "") {
        Console.WriteLine($"PROMO! Je geniet van de promo *** {promoMsg} ***");
      }
      return totPrijs - korting;
    }

    private static Frisdranken BestelFrisdrank() {
      Console.Write(
        "Frisdranknummer?\ngeen - 0\nwater = 1\nfanta = 2\ncola = 3\n");
      int num = Convert.ToInt32(Console.ReadLine());
      return (Frisdranken)(num - 1);
    }

    private static int BestelFrietjes() {
      Console.Write("Aantal frietjes: ");
      return Convert.ToInt32(Console.ReadLine());
    }
  }
}